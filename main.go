package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"os"
	"strings"
	"time"
)

const defaultTime = 30

type ConfigFlags struct {
	FileName string
	Time     int
	Shuffle  bool
}

func main() {
	config := parseFlags()

	records, err := readCsvFile(config.FileName)
	if err != nil {
		log.Fatal(err)
	}
	var goodAnswers int

	ticker := time.NewTicker(time.Duration(config.Time) * time.Second)
	done := make(chan bool)

	go func() {
		for {
			select {
			case <-done:
				return
			case <-ticker.C:
				printData(goodAnswers, len(records), "\nEnd of time.\n")
				// probably there's a better way to end a ticker
				os.Exit(0)
			}
		}
	}()

	answerQuestions(records, &goodAnswers, config.Shuffle) // probably an anti-pattern
	done <- true
	ticker.Stop()
	printData(goodAnswers, len(records))
}

func parseFlags() ConfigFlags {
	fileName := flag.String("file", "problems.csv", "The file to get the questions")
	seconds := flag.Int("time", defaultTime, "The time expected to not exced to answer the whole questions")
	shuffle := flag.Bool("shuffle", false, "Set this flag to true if you want to shuffle the questions")
	flag.Parse()

	return ConfigFlags{
		FileName: *fileName,
		Time:     *seconds,
		Shuffle:  *shuffle,
	}
}

func printData(goodAnswers, totalRecords int, format ...string) {
	for _, f := range format {
		fmt.Print(f)
	}
	fmt.Printf("Correct answers: %d\n", goodAnswers)
	fmt.Printf("Total answers: %d\n", totalRecords)
}

func readCsvFile(filePath string) ([][]string, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	csvReader := csv.NewReader(file)
	records, err := csvReader.ReadAll()
	if err != nil {
		return nil, err
	}

	return records, nil
}

func getArraySequence(numberOfRecords int, shuffle bool) []int {
	array := make([]int, numberOfRecords)
	for i := 0; i < numberOfRecords; i++ {
		array[i] = i + 1
	}
	if shuffle {
		s1 := rand.NewSource(time.Now().UnixNano())
		r1 := rand.New(s1)
		r1.Shuffle(
			numberOfRecords,
			func(i, j int) {
				array[i], array[j] = array[j], array[i]
			},
		)
	}

	return array
}

func answerQuestions(records [][]string, goodAnswers *int, shuffle bool) {
	sequence := getArraySequence(len(records), shuffle)

	for _, numberOfSequence := range sequence {
		fmt.Printf("Question: %s. Answer: ", records[numberOfSequence-1][0])
		var answer string
		_, err := fmt.Scanf("%s", &answer)
		if err != nil {
			log.Fatal(err)
		}
		if strings.EqualFold(answer, records[numberOfSequence-1][1]) {
			*goodAnswers++
		}
	}
}
